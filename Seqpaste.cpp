// Seqpaste.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Seqpaste.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HFONT hFontCurrent;
std::vector<std::wstring> lines;
int index = 0;
bool currentIsLoaded = false;
HWND hwnd;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_SEQPASTE, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

	hFontCurrent = CreateFont(24, 0, 0, 0, FW_REGULAR, FALSE, FALSE, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, VARIABLE_PITCH, L"Arial");

	std::wstring sequence = L"";

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_SEQPASTE));
    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SEQPASTE));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName   = nullptr;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowExW(WS_EX_TOPMOST, szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 300, 100, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	DragAcceptFiles(hWnd, TRUE);
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	BOOL success;
	success = RegisterHotKey(hWnd, PASTE_HOTKEY, MOD_WIN | MOD_ALT, 'V');
	if (!success) {
		DWORD error = GetLastError();
		MessageBox(hWnd, (L"Cannot register hotkey error " + std::to_wstring(error)).c_str(), _T("Error"), MB_ICONWARNING);
	}
	RegisterHotKey(hWnd, NEXT_HOTKEY, MOD_WIN | MOD_ALT, 'B');
	RegisterHotKey(hWnd, NEXT_HOTKEY, MOD_WIN | MOD_ALT, 'N');
	RegisterHotKey(hWnd, PREV_HOTKEY, MOD_WIN | MOD_ALT, 'C');
	hwnd = hWnd;

	return TRUE;
}

void CurrentToClipboard() {
	if (lines.empty()) return;
	size_t length = (lines[index].size() + 1)*2;
	HGLOBAL hMem = GlobalAlloc(GMEM_MOVEABLE, length);
	memcpy(GlobalLock(hMem), lines[index].c_str(), length);
	GlobalUnlock(hMem);
	OpenClipboard(nullptr);
	EmptyClipboard();
	SetClipboardData(CF_UNICODETEXT, hMem);
	CloseClipboard();
	InvalidateRect(hwnd, nullptr, TRUE);
}

void EnsureCurrentLoaded() {
	if (currentIsLoaded) return;
	CurrentToClipboard();
	currentIsLoaded = true;
}

void HandlePaste(HWND hWnd) {
	EnsureCurrentLoaded();

	INPUT ip;
	ip.type = INPUT_KEYBOARD;
	ip.ki.wScan = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;

	// Release "Win" key
	// TODO check if Win is down first
	ip.ki.wVk = VK_LWIN;
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
	// Release "Alt" key
	ip.ki.wVk = VK_MENU;
	SendInput(1, &ip, sizeof(INPUT));

	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = 0; // 0 for key press
	SendInput(1, &ip, sizeof(INPUT));

	ip.ki.wVk = 'V';
	SendInput(1, &ip, sizeof(INPUT));
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));

	// Release the "Ctrl" key
	ip.ki.wVk = VK_CONTROL;
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));

	// Press "Win" key again
	ip.ki.wVk = VK_LWIN;
	ip.ki.dwFlags = 0;
	SendInput(1, &ip, sizeof(INPUT));
	ip.ki.wVk = VK_MENU;
	SendInput(1, &ip, sizeof(INPUT));

	index += (index >= lines.size()-1) ? 0 : 1;
	currentIsLoaded = false;
	SetTimer(hwnd, TIMER_DELAY_LOAD, 1000, nullptr);
	InvalidateRect(hwnd, nullptr, TRUE);
}

void OnPaint(HWND hWnd) {
	PAINTSTRUCT ps;
	HDC hdc = BeginPaint(hWnd, &ps);
	SaveDC(hdc);
	SetBkMode(hdc, TRANSPARENT);
	RECT rect;
	SelectBrush(hdc, GetStockBrush(BLACK_BRUSH));
	GetClientRect(hWnd, &rect);
	Rectangle(hdc, 0, 0, rect.right, rect.bottom);
	int x = rect.right/2;

	SelectFont(hdc, hFontCurrent);
	SetTextAlign(hdc, TA_CENTER);

	if (lines.empty()) {
		SetTextColor(hdc, 0xB0B0F0);
		std::wstring text = L"-- drop file here --";
		TextOut(hdc, x, 5, text.c_str(), int(text.length()));
	} else {
		SetTextColor(hdc, 0xF0F0F0);
		const std::wstring & text = lines[index];
		TextOut(hdc, x, 5, text.c_str(), int(text.length()));

		SetTextColor(hdc, 0xB0B0B0);
		std::wstring postext = std::to_wstring(index + 1);
		postext += L"/" + std::to_wstring(lines.size());
		TextOut(hdc, x, 40, postext.c_str(), int(postext.length()));
	}

	RestoreDC(hdc, -1);
	EndPaint(hWnd, &ps);
}



void HandleDropFiles(HDROP hDrop) {
	const int MAX_SIZE = 256;
	TCHAR filename[MAX_SIZE];
	DragQueryFile(hDrop, 0, filename, MAX_SIZE);
	HANDLE hFile = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
	const int BUFF_SIZE = 4096;
	std::string utf8;
	char buffer[BUFF_SIZE];
	DWORD readBytes;
	do {
		BOOL success = ReadFile(hFile, buffer, BUFF_SIZE-1, &readBytes, nullptr);
		if (!success || readBytes == 0) break;
		buffer[readBytes] = '\0';
		utf8.append(buffer, readBytes);
	} while (false); // prevent too much data
    CloseHandle(hFile);
	std::vector<wchar_t> wstr(utf8.size()+1);
	toUTF16(utf8.data(), wstr.data(), int(utf8.length()));
	std::wstringstream wss(wstr.data());
	std::wstring line;
	lines.clear();
	index = 0;
	while (std::getline(wss, line)) {
		if (line[line.size()-1] == L'\r') {
			line.erase(line.size()-1);
		}
		if (line.size() == 0) continue;
		lines.push_back(line);
	}
	if (lines.size() == 0) return;
	CurrentToClipboard();
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_HOTKEY:
		switch(wParam) {
		case PASTE_HOTKEY:
			HandlePaste(hWnd);
			break;
		case NEXT_HOTKEY:
			index += (index >= lines.size()-1) ? 0 : 1;
			CurrentToClipboard();
			break;
		case PREV_HOTKEY:
			index -= (index <= 0) ? 0 : 1;
			CurrentToClipboard();
		}
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
		OnPaint(hWnd);
        break;
	case WM_DROPFILES:
		HandleDropFiles(HDROP(wParam));
		break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
	case WM_TIMER: {
		const WPARAM timerID = wParam;
		if (timerID == TIMER_DELAY_LOAD) {
			EnsureCurrentLoaded();
			KillTimer(hwnd, TIMER_DELAY_LOAD);
		}
		break; }
	case WM_RBUTTONDOWN:
		MessageBox(hWnd, L"Drop text file with items separated by a newline.\n"
			"Then use [Win]+[Alt]+[V] to paste and load next entry.\n"
			"Navigate through entries with [Win]+[Alt]+[C] and [Win]+[Alt]+[B] or +[N]", L"Usage", MB_OK);
		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
