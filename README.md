# SeqPast #

Sequential clipboard paste. 

Allows for inserting file content in a sequential matter, one line after another. Can be used for example for filling multiple form fields.

Demo: https://www.youtube.com/watch?v=2cbHGKALIso

## Usage ##
Drop a text file with items separated by a newline. 

Then use [Win]+[Alt]+[V] to paste and load next entry.

Navigate through entries with [Win]+[Alt]+[C] and [Win]+[Alt]+[B] or +[N]
