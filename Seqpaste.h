#pragma once

#include "resource.h"
#define PASTE_HOTKEY	1000
#define NEXT_HOTKEY		1001
#define PREV_HOTKEY		1002

#define toUTF8(utf16, utf8, size) WideCharToMultiByte(CP_UTF8, 0, utf16, -1, utf8, size, NULL, NULL)
#define toUTF16(utf8, utf16, size) MultiByteToWideChar(CP_UTF8, 0, utf8, -1, utf16, size)

#define WMX_LOAD_CLIPBOARD	(WM_USER + 1)
#define TIMER_DELAY_LOAD	101